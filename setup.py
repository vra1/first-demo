from setuptools import setup
import unittest

def my_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern='test_*.py')
    return test_suite

setup(name="my_print", version="1.0", py_modules=['my_print'], test_suite="setup.my_test_suite")
